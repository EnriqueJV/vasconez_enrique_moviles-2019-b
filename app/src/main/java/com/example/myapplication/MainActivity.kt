package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

        lateinit var startButton : Button
        lateinit var tapMeButton : Button
        lateinit var  scoreTextView: TextView

        private lateinit var countDownTimer: CountDownTimer
        private  var InitialCountDown :Long =10000
        private var countDownInterval: Long =1000

        private var timeLeft = 10
        private var gameScore = 0

        private var isGameStarted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tapMeButton = findViewById(R.id.tap_me_button)
        startButton = findViewById(R.id.start_button)
        scoreTextView = findViewById(R.id.score_text_view)

        tapMeButton.setOnClickListener { incrementScore() }
    }

    private fun incrementScore(){
        if (!isGameStarted){
            startGame()
        }
        gameScore ++
        scoreTextView.text = getString(R.string.score, gameScore)
    }

    private fun startGame(){
        countDownTimer.start()
        isGameStarted = true
    }

    private  fun resetGame(){
        gameScore = 0
        scoreTextView.text =getString(R.string.score, gameScore)

        timeLeft = 10

        configCountDownTimer()
        isGameStarted = false
   }

    private fun endGame(){
        Toast.makeText(this,getString(R.string.game_over,gameScore),Toast.LENGTH_LONG).show()
        resetGame()
    }

    private fun configCountDownTimer(){
        countDownTimer =object: CountDownTimer((timeLeft * 1000).toLong(),countDownInterval){
            override fun onFinish() {
                endGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt()/ 1000
            }

        }
    }
}
